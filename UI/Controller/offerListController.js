﻿
selling.controller('offerListController', function ($scope, $http, $modal, $log, $timeout) {
    //$scope.selectOffers = $http.get("http://localhost:8080/REAgency/webapi/offer/offers/" + 2)
    //     .success(function (response) { $scope.offers = response; }); 
    $scope.details = false;
    $scope.result = false;
    $scope.search = true;
    $scope.findOffers = function (sellingOfferId) {
        $scope.selectedSellingOfferId = sellingOfferId;
        $scope.selectOffers = $http.get("http://localhost:8080/REAgency/webapi/offer/offers/" + sellingOfferId)
           .success(function (response) {
               $scope.offers = response;
               $scope.details = true;
           });
        
    };

    $scope.acceptOffer = function () {
        var selectedOfferId = $scope.selected;
        var sellingOfferId = $scope.selectedSellingOfferId;
        $http.post("http://localhost:8080/REAgency/webapi/offer/accept/" + sellingOfferId + "/" + selectedOfferId)
                        .success(function (response) {
                            responseResult = response;
                            $scope.details = false;
                            $scope.result = true;
                            $scope.search = false;
                        });
       

    };
    $scope.selectOffer = function (offer) {
        $scope.selectedOffer = offer;
    };
})