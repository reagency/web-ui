﻿
var selling = angular.module('teamzero', ['kendo.directives', 'ngRoute', 'ui.bootstrap']);//,'ngAnimate',, 'google-maps'


selling.config(function ($routeProvider) {


    $routeProvider
        .when('/', {
            controller: 'TypeController',
            templateUrl: 'View/settingView.html'
        })
        .when('/setting', {
            controller: 'TypeController',
            templateUrl: 'View/settingView.html'
        })
        .when('/addEstate', {
            controller: 'sellingOfferController',
            templateUrl: 'View/sellingOfferView.html'
        })
         .when('/offers', {
             controller: 'offerListController',
             templateUrl: 'View/offerListView.html'
         })
         .when('/sellinglist', {
             controller: 'sellingOfferListController',
             templateUrl: 'View/sellingOfferListView.html'
         })
        .when('/about', {
            templateUrl: 'about.html'
        });


});



