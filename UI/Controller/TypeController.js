﻿var tId = '';

selling.controller('TypeController', function ($scope, $http, $modal, $log) {


    //$scope.customerType = '';
    $scope.trypegrid = false;
    $scope.selectType = function (t) {
        $scope.option = t;
        $http.get("http://localhost:8080/REAgency/webapi/type/" + t)
               .success(function (response) {
                   $scope.customers = response;
                 
               });
        $scope.trypegrid = true;
        
    };

    $scope.open = function (type, id) {
        //lg, sm , , size types
        $scope.customerId = id;
        tId = $scope.customerId
        $scope.customerType = type;
        var updatedtype = { id: $scope.customerId, type: $scope.customerType };
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm',
            resolve: {
                items: function () {
                    return $scope.customerType;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.customerType = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.openNew = function (optionType) {
        //lg, sm , , size types
        $scope.option = optionType;
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlNewType',
            size: 'sm',
            resolve: {
                items: function () {
                    return { 'type': $scope.option, 'newtype': $scope.customerType };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.customerType = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.mailsenderdiv = true;
    $scope.serverInfodiv = true;
    $scope.messagediv1 = false;
    $scope.messagediv2 = false;
    $scope.sortBy = 'type';
    $scope.reverse = false;
    //$scope.customers = getCustomers();
    $scope.doSort = function (propName) {
        $scope.sortBy = propName;
        $scope.reverse = !$scope.reverse
    };
    $scope.DeleteRow = function (index, typeId) {

        $scope.customers.splice(index, 1);
        var x = typeId;
        $http.get("http://localhost:8080/REAgency/webapi/type/delete/" + x)
                           .success(function (response) { });
    }
    $scope.EditRow = function (type) {


        //type.type = $scope.customerType
        //$http.post("http://localhost:8080/REAgency/webapi/type/update/type", type)
        //                   .success(function (response) { });
        ////$scope.showModal = false;
    };
    $scope.RefreshtypeGrid = function (selectedType) {
        $http.get("http://localhost:8080/REAgency/webapi/type/" + selectedType)
             .success(function (response) { $scope.customers = response; });
    };
    $scope.mailSender = function () {
        var email = $scope.email;
        var password = $scope.password;
      
        var obj = {};
        obj.email = email + "@gmail.com";
        obj.password = password;
        $http.post("http://localhost:8080/REAgency/webapi/server/update/sender", obj)
                           .success(function (response) { });
        $scope.mailsenderdiv = false;
        $scope.messagediv1 = true;
        
    };
    $scope.offersSetter = function () {
        var offerExpiry = $scope.offerExpiry;
        var sellingExpiry = $scope.sellingExpiry;

        var obj = {};
        obj.offerExpiry = offerExpiry;
        obj.sellingExpiry = sellingExpiry;
        $http.post("http://localhost:8080/REAgency/webapi/server/update/offersetting", obj)
                           .success(function (response) { });
        $scope.serverInfodiv = false;
        $scope.messagediv2 = true;
    };

	})

.controller('ModalInstanceCtrl', function ($scope, $http, $modalInstance, items) {

    $scope.customerType = items;
    $scope.selected = $scope.customerType

    $scope.ok = function () {

        
        var updatedtype = { id: tId, type: $scope.customerType };
        $http.post("http://localhost:8080/REAgency/webapi/type/update/type", updatedtype)
                           .success(function (response) { });

       
        $modalInstance.close($scope.customerType);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
})
.controller('ModalInstanceCtrlNewType', function ($scope, $http, $modalInstance, items) {
    $scope.customerType = items;
    $scope.selected = $scope.customerType
   
    
    var t = items.type;
    $scope.customerType = items.newtype;
    $scope.selectedOption = t
    $scope.ok = function () {

        
        var mustadded = { id: 0, type: $scope.customerType };
        $http.post("http://localhost:8080/REAgency/webapi/type/" + t + "/newtype", mustadded)
                           .success(function (response) { });
      
       
        $modalInstance.close($scope.customerType);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
