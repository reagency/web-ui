﻿//var selling = angular.module('sellingOffer', ['kendo.directives', 'ui.bootstrap'])//, 'ngAnimate'

selling.controller('sellingOfferController', function ($scope, $http, $modal, $log, $timeout) {

    $scope.iCnt = 1;
    $scope.estateRegistration = true;
    $scope.saveResult = false;
    $scope.selectCountry = $http.get("http://localhost:8080/REAgency/webapi/location/countries")
               .success(function (response) { $scope.countries = response; });

    $scope.seslectEstetType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateType")
               .success(function (response) { $scope.estateTypes = response; });
    $scope.selectImageType = $http.get("http://localhost:8080/REAgency/webapi/type/ImageType")
               .success(function (response) { $scope.imageTypes = response; });
    $scope.selectApplianceType = $http.get("http://localhost:8080/REAgency/webapi/type/ApplianceType")
      .success(function (response) { $scope.applianceTypes = response; });
    $scope.selectPieceType = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
          .success(function (response) { $scope.pieceTypes = response; });

    $scope.selectsellingType = $http.get("http://localhost:8080/REAgency/webapi/type/SellingOfferType")
         .success(function (response) { $scope.sellingOfferTypes = response; });


    $scope.countrySelected = function (id) {
        $scope.countyId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/state/" + id)
               .success(function (response) { $scope.states = response; });
    }
    $scope.stateSelected = function (id) {
        $scope.stateId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/city/" + id)
               .success(function (response) { $scope.cities = response; });
    }
    $scope.citySelected = function (id) {
        $scope.cityId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/district/" + id)
               .success(function (response) { $scope.districts = response; });
    }
    $scope.districtSelected = function (id) {
        $scope.districtId = id;
    }
    $scope.seslectFeatureType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateFeatureType")
              .success(function (response) { $scope.featureTypes = response; });
    $scope.seslectFacilityType = $http.get("http://localhost:8080/REAgency/webapi/type/FacilityType")
             .success(function (response) { $scope.facilityTypes = response; });
    //$scope.featureType = 
    $scope.featureTypeSelected = function (id) {
        //    $scope.featureType = id;
    }
    $scope.estateTypeSelected = function (id) {
        $scope.estateTypeId = id;
    }
    $scope.renewDateSelected = function (date) {
        $scope.renewDate = date;
    }
    $scope.monthSelectorOptions = {
        start: "year",
        depth: "year"
    };
    $scope.getType = function (x) {
        return typeof x;
    };
    $scope.isDate = function (x) {
        return x instanceof Date;
    };


    // Create a counter to keep track of the additional telephone inputs



    ///////////////////////////////////feature
    $scope.estateImages = [];
    $scope.featurestmp = [];
    $scope.DeletefRow = function (f) {
        $scope.featurestmp = $scope.featurestmp
               .filter(function (element) {
                   return element.index !== f.index;
               });
    };
    $scope.openNew = function () {
        //lg, sm , , size types
        fet = $http.get("http://localhost:8080/REAgency/webapi/type/EstateFeatureType")
              .success(function (response) { $scope.featureTypes = response; });
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrlEstateFeature',
            size: 'sm',
            resolve: {
                items: function () {
                    return { 'types': $scope.featureTypes, 'dataset': $scope.featurestmp };
                }
            }
        });

        modalInstance.result.then(function (fet) {
            $scope.featureTypes = fet;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    /////////////////////////////////////////////// nearby
    $scope.nearbytmp = [];
    $scope.DeletenRow = function (n) {
        $scope.nearbytmp = $scope.nearbytmp
               .filter(function (element) {
                   return element.index !== n.index;
               });
    };
    $scope.openNewNearby = function () {
        //lg, sm , , size types
        net = $http.get("http://localhost:8080/REAgency/webapi/type/FacilityType")
              .success(function (response) { $scope.facilityTypes = response; });
        var modalInstance = $modal.open({
            templateUrl: 'myModalNContent.html',
            controller: 'ModalInstanceCtrlNewNearby',
            size: 'sm',
            resolve: {
                items: function () {
                    return { 'types': $scope.facilityTypes, 'dataset': $scope.nearbytmp };
                }
            }
        });

        modalInstance.result.then(function (net) {
            $scope.facilityTypes = net;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    ///////////////////////////////////////save Estate
    $scope.saveEstate = function () {
        var stateId = 0;
        var cityId = 0;
        var districtId = 0;
        var street = "";
        var number ="";
        var postalcode = "";
        var estateType = null;
        var renewDate = null;
        var age = 0;
        var discription = "";
        var basePrice = 0;
        var sellingOfferType = null;
        var sellingExtraInfo = "";
        var featurestmp = [];
        var nearbytmp = [];
        var imagesList = [];
        var units = [];
        var countryId = 0;
        if ($scope.country != null) {
            var countryId = $scope.country.id;
        };
        if ($scope.state != null) {
            var stateId = $scope.state.id;
        };
        if ($scope.city != null) {
            var cityId = $scope.city.id;
        };
        if ($scope.district != null) {
            var districtId = $scope.district.id;
        };
        var street = $scope.street;
        var number = $scope.number;
        var postalcode = $scope.postalcode;
        var estateType = $scope.estateType;
        var renewDate = $scope.renewDate;
        var age = parseInt($scope.age);
        var discription = $scope.estateExtraInfo;
        var basePrice = parseInt($scope.baseprice);
        var sellingOfferType = $scope.sellingOfferType;
        var sellingExtraInfo = $scope.sellingExtraInfo;
        var featurestmp = $scope.featurestmp;
        var nearbytmp = $scope.nearbytmp;
        var imagesList = $scope.estateImages;
        var units = $scope.units;

       
        var estateObj = {
            countryId: countryId,
            stateId: stateId,
            cityId: cityId,
            districtId: districtId,
            street: street,
            number: number,
            postalcode: postalcode,
            estateType: estateType,
            renewDate: renewDate,
            age: age,
            discription: discription,
            basePrice: basePrice,
            sellingOfferType: sellingOfferType,
            sellingExtraInfo: sellingExtraInfo,
            features: featurestmp,
            facilities: nearbytmp,
            images: imagesList,
            units: units
        };
       
        var responseResult;
        
        $http.post("http://localhost:8080/REAgency/webapi/estate/newEstate", estateObj)
                          .success(function (response) {
                              $scope.sellingOfferTrackId = response;
                              $scope.estateRegistration = false;
                              $scope.saveResult = true;
                          });
        //while (responseResult == null) {

        //}
        //$scope.sellingOfferTrackId = responseResult;
       

    };
    ////////////////////////////////////////////////////estate images
    $scope.uniImages = [];
    $scope.units = [];
    $scope.deleteUnit = function (u) {
        $scope.units = $scope.units.filter(function (element) {
            return element.index != u.index;
        });
    };
    $scope.deleteEstateImage = function (f) {
        $scope.estateImages = $scope.estateImages
               .filter(function (element) {
                   return element.image !== f.image;
               });
    };
    $scope.showEstateImage = function (image) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalEstateImageContent.html',
            controller: 'ModalInstanceCtrlshowEstateImage',
            size: 'lg',
            resolve: {
                items: function () {
                    return { 'image': image };
                }
            }
        });

        modalInstance.result.then(function () {
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.openNewEstateImage = function () {
        iet = $http.get("http://localhost:8080/REAgency/webapi/type/ImageType")
               .success(function (response) { $scope.imageTypes = response; });
        var modalInstance = $modal.open({
            templateUrl: 'myModalAddEstateImageContent.html',
            controller: 'ModalInstanceCtrlnewEstateImage',
            size: 'sm',
            resolve: {
                items: function () {
                    return { 'image': $scope.estateImages, 'imagetypes': $scope.imageTypes };
                }
            }
        });

        modalInstance.result.then(function (iet) {
            $scope.facilityTypes = iet;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.openNewUnit = function () {
        iet = $http.get("http://localhost:8080/REAgency/webapi/type/ImageType")
               .success(function (response) { $scope.imageTypes = response; });
        aet = $http.get("http://localhost:8080/REAgency/webapi/type/ApplianceType")
              .success(function (response) { $scope.applianceTypes = response; });
        pet = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
              .success(function (response) { $scope.pieceTypes = response; });

        var modalInstance = $modal.open({
            templateUrl: 'myModalAddUnitContent.html',
            controller: 'ModalInstanceCtrlnewUnit',
            size: 'lg',
            resolve: {
                items: function () {
                    return { 'units': $scope.units, 'piecetypes': $scope.pieceTypes, 'appliancetypes': $scope.applianceTypes, 'imagetypes': $scope.imageTypes, 'unitimages': $scope.uniImages, 'editIndex': -1 };
                }
            }
        });

        modalInstance.result.then(function (iet, aet, pet) {
            $scope.facilityTypes = iet;
            $scope.applianceTypes = aet;
            $scope.pieceTypes = pet;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.editUnit = function (index) {

        iet = $http.get("http://localhost:8080/REAgency/webapi/type/ImageType")
             .success(function (response) { $scope.imageTypes = response; });
        aet = $http.get("http://localhost:8080/REAgency/webapi/type/ApplianceType")
              .success(function (response) { $scope.applianceTypes = response; });
        pet = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
              .success(function (response) { $scope.pieceTypes = response; });

        var modalInstance = $modal.open({
            templateUrl: 'myModalAddUnitContent.html',
            controller: 'ModalInstanceCtrlnewUnit',
            size: 'lg',
            resolve: {
                items: function () {
                    return { 'units': $scope.units, 'piecetypes': $scope.pieceTypes, 'appliancetypes': $scope.applianceTypes, 'imagetypes': $scope.imageTypes, 'unitimages': $scope.uniImages, 'editIndex': index };
                }
            }
        });

        modalInstance.result.then(function (iet, aet, pet) {
            $scope.facilityTypes = iet;
            $scope.applianceTypes = aet;
            $scope.pieceTypes = pet;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
})
.directive('hboTabs', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            var jqueryElm = $(elm[0]);
            $(jqueryElm).tabs()
        }
    };
})
.controller('ModalInstanceCtrlNewNearby', function ($scope, $http, $modalInstance, items) {


    $scope.facilityTypes = items.types;
    $scope.nearbytmp = items.dataset;

    $scope.okN = function (t, value, unit) {

        var x = $scope.nearbytmp.length
        var mustadded = { facilityType: t, value: value, unit: unit, index: x };

        $scope.nearbytmp.push(mustadded);

        $modalInstance.close($scope.facilityTypes, $scope.nearbytmp);
    };

    $scope.cancelN = function () {
        $modalInstance.dismiss('cancel');
    };
})
.controller('ModalInstanceCtrlEstateFeature', function ($scope, $http, $modalInstance, items) {


    $scope.featureTypes = items.types;
    $scope.featurestmp = items.dataset;

    $scope.ok = function (t, Quantity) {

        var x = $scope.featurestmp.length
        var mustadded = { featureType: t, quantity: Quantity, index: x };

        $scope.featurestmp.push(mustadded);

        $modalInstance.close($scope.featureTypes, $scope.featurestmp);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
})
.controller('ModalInstanceCtrlshowEstateImage', function ($scope, $modalInstance, items) {
    $scope.image = items.image;
})
.controller('ModalInstanceCtrlnewEstateImage', function ($scope, $modalInstance, items) {
    $scope.estateImages = items.image;
    $scope.imageTypes = items.imagetypes;
    $scope.okEstateImage = function (t) {
        //var fd = new FormData()

        //fd.append("uploadedFile", $scope.files)
        //var xhr = new XMLHttpRequest()
        //xhr.open("POST", "/fileupload")
        //xhr.send(fd)

        






        var x = $scope.estateImages.length
        if (x > 0) {
            $scope.estateImages[x - 1].type = t;
        }
        
        $modalInstance.close($scope.estateImages, $scope.imageTypes);
    };

    $scope.cancel = function () {
        var x = $scope.estateImages.length;
        if ($scope.estateImages[x - 1].type == null) {
            $scope.estateImages = $scope.estateImages.filter(function (element) {
                return element.type !== null;
            });
        }
        $modalInstance.close($scope.estateImages, $scope.imageTypes)
        $modalInstance.dismiss('cancel');
    };
})
.controller('ModalInstanceCtrlnewUnit', function ($scope,$http, $modalInstance, items) {


    $scope.selectApplianceType = $http.get("http://localhost:8080/REAgency/webapi/type/ApplianceType")
  .success(function (response) { $scope.applianceTypes = response; });
    $scope.selectPieceType = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
          .success(function (response) { $scope.pieceTypes = response; });

    $scope.unitRenewDateSelected = function (date) {
        $scope.unitRenewDate = date;
    }
    $scope.piecevisible = false;
    $scope.saveButtonShow = false;
    $scope.addButtonShow = true;

    $scope.openNewPiece = function () {
        $scope.piecevisible = true;
    };
    $scope.piecetmp = [];

    ///////////////////////////////
    $scope.unitimagevisible = false;
    $scope.saveApplianceButtonShow = false;
    $scope.addApplianceButtonShow = true;

    $scope.openNewAppliance = function () {
        $scope.appliancevisible = true;
    };
    $scope.appliancetmp = [];
    //////////////////////////images
    $scope.appliancevisible = false;
    $scope.saveUnitImageButtonShow = false;
    $scope.addUnitImageButtonShow = true;
    $scope.sliderunitImageIndex = 0;
    $scope.sliderImage;
    $scope.unitImagetmp = [];
 

    // show prev image
    $scope.showPrev = function () {
        if ($scope.sliderunitImageIndex > 0) {
            $scope.sliderunitImageIndex--;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        } else {
            $scope.sliderunitImageIndex = $scope.unitImagetmp.length - 1;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        }
    };
    //showUnitImage(um)
    // show next image
    $scope.showNext = function () {
        if ($scope.sliderunitImageIndex < $scope.unitImagetmp.length - 1) {
            $scope.sliderunitImageIndex++;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        } else {
            $scope.sliderunitImageIndex = 0;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        }
    };


  

    //////////////////////

    $scope.units = items.units;
    $scope.pieceTypes = items.piecetypes;
    $scope.applianceTypes = items.appliancetypes;
    $scope.imageTypes = items.imagetypes;
    $scope.uniImages = items.unitimages;
    $scope.editedUnitFlag = items.editIndex;

    if (items.editIndex != -1) {
        var unit = $scope.units[items.editIndex];
        if (unit.unitnumber != null) {
            $scope.unitnumber = unit.unitnumber;
        }
        if (unit.floornumber != null) {
            $scope.floornumber = unit.floornumber;
        }
        if (unit.unitRenewDate != null) {
            $scope.unitRenewDate = unit.unitRenewDate;
        }
        if (unit.unitExtraInfo != null) {
            $scope.unitExtraInfo = unit.unitExtraInfo;
        }
        if (unit.pieces != null) {
            $scope.piecetmp = unit.pieces;
        }
        if (unit.appliances != null) {
            $scope.appliancetmp = unit.appliances;
        }
        if (unit.unitImages != null) {
            $scope.unitImagetmp = unit.unitImages;
        }
    }
    $scope.SaveNewPiece = function (pieceType, pieceExtraInfo, pieceQ) {
        var index = $scope.piecetmp.length;
        var piece = { 'pieceType': pieceType, 'pieceExtraInfo': pieceExtraInfo, 'pieceQ': pieceQ, 'index': index };
        $scope.piecetmp.push(piece);
        $scope.saveButtonShow = false;
        $scope.piecevisible = false;
        $scope.addButtonShow = true;
        $scope.pieceType = null;
        $scope.pieceExtraInfo = null;
        $scope.pieceQ = null;
    };
    $scope.openNewPiece = function () {
        $scope.saveButtonShow = true;
        $scope.piecevisible = true;
        $scope.addButtonShow = false;

    };
    $scope.SaveNewAppliance = function (applianceType, applianceExtraInfo, applianceQ) {
        var index = $scope.appliancetmp.length;
        var appliance = { 'applianceType': applianceType, 'applianceExtraInfo': applianceExtraInfo, 'applianceQ': applianceQ, 'index': index };
        $scope.appliancetmp.push(appliance);
        $scope.saveApplianceButtonShow = false;
        $scope.appliancevisible = false;
        $scope.addApplianceButtonShow = true;
        $scope.applianceType = null;
        $scope.applianceExtraInfo = null;
        $scope.applianceQ = null;
    };
    $scope.openNewAppliance = function () {
        $scope.saveApplianceButtonShow = true;
        $scope.appliancevisible = true;
        $scope.addApplianceButtonShow = false;

    };
    $scope.DeleteaRow = function (ai) {
        $scope.appliancetmp = $scope.appliancetmp.filter(function (element) {
            return element.index != ai.index;
        });
    };
    $scope.DeletepRow = function (pi) {
        $scope.piecetmp = $scope.piecetmp.filter(function (element) {
            return element.index != pi.index;
        });
    };
    $scope.SaveNewUnitImage = function () {
        $scope.saveUnitImageButtonShow = false;
        $scope.unitimagevisible = false;
        $scope.addUnitImageButtonShow = true;
        $scope.unitimageType = null;
    };

    $scope.openNewUnitImage = function () {
        $scope.unitimagevisible = true;
        $scope.saveUnitImageButtonShow = true;
        $scope.unitimagevisible = true;
        $scope.addUnitImageButtonShow = false;
    };

    $scope.okUnitImage = function (t) {
        //var fd = new FormData()

        //fd.append("uploadedFile", $scope.files)
        //var xhr = new XMLHttpRequest()
        //xhr.open("POST", "/fileupload")
        //xhr.send(fd)
        if ($scope.unitImagetmp.length > 0) {
            var x = $scope.unitImagetmp.length
            $scope.unitImagetmp[x - 1].type = t;
        }
        $scope.slidervisible = true;
    };
    $scope.slidervisible = false;
    $scope.showUnitImage = function (um) {
        $scope.sliderunitImageIndex = um.index;
        $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
    };//
    $scope.deleteUnitImage = function (um) {
        $scope.unitImagetmp = $scope.unitImagetmp.filter(function (element) {
            return element.index != um.index;
        });
        if ($scope.unitImagetmp.length < 1) {
            $scope.slidervisible = false;
        }
    };
    
    $scope.okU = function () {
        //Edit part
        if ($scope.editedUnitFlag != -1) {
            var unit = $scope.units[$scope.editedUnitFlag];
            var index = $scope.units.length;
            if (unit.unitnumber != $scope.unitnumber) {
                unit.unitnumber = $scope.unitnumber;
            }
            if (unit.floornumber != $scope.floornumber) {
                unit.floornumber = $scope.floornumber;
            }
            if (unit.unitRenewDate != $scope.unitRenewDate) {
                unit.unitRenewDate = $scope.unitRenewDate;
            }
            if (unit.unitExtraInfo != $scope.unitExtraInfo) {
                unit.unitExtraInfo = $scope.unitExtraInfo;
            }
            unit.pieces = $scope.piecetmp;
            unit.appliances = $scope.appliancetmp;
            unit.unitImages = $scope.unitImagetmp;
            $scope.units[unit.index] = unit;

        } else {// add new
            var unitnumber;
            var floornumber;
            var unitRenewDate;
            var unitExtraInfo;
            var pieces;
            var appliances;
            var unitImages;
            var index;
            var index = $scope.units.length;
            if ($scope.unitnumber != null) {
                unitnumber = $scope.unitnumber;
            }
            if ($scope.floornumber != null) {
                floornumber = $scope.floornumber;
            }
            if ($scope.unitRenewDate != null) {
                unitRenewDate = $scope.unitRenewDate;
            }
            if ($scope.unitExtraInfo != null) {
                unitExtraInfo = $scope.unitExtraInfo;
            }
            if ($scope.piecetmp != null) {
                pieces = $scope.piecetmp;
            }
            if ($scope.appliancetmp != null) {
                appliances = $scope.appliancetmp;
            }
            if ($scope.unitImagetmp != null) {
                unitImages = $scope.unitImagetmp;
            }
            index = index;
            var unit = {
                'unitnumber': unitnumber, 'floornumber': floornumber, 'unitRenewDate': unitRenewDate,
                'unitExtraInfo': unitExtraInfo, 'pieces': pieces, 'appliances': appliances,
                'unitImages': unitImages, 'index': index
            }
            $scope.units.push(unit);
        };

        //var fd = new FormData()

        //fd.append("uploadedFile", $scope.files)
        //var xhr = new XMLHttpRequest()
        //xhr.open("POST", "/fileupload")
        //xhr.send(fd)
        //var x = $scope.units.length
        //$scope.estateImages[x - 1].type = t;
        $modalInstance.close($scope.units, $scope.pieceTypes, $scope.applianceTypes, $scope.imageTypes, $scope.uniImages, $scope.piecetmp);
    };

    $scope.cancelU = function () {
        //var x = $scope.estateImages.length;
        //if ($scope.estateImages[x - 1].type == null) {
        //    $scope.estateImages = $scope.estateImages.filter(function (element) {
        //        return element.type !== null;
        //    });
        //}
        $modalInstance.close($scope.estateImages, $scope.imageTypes, $scope.saveButtonShow, $scope.piecevisible, $scope.piecetmp)
        $modalInstance.dismiss('cancel');
    };
})
.directive('myUpload', [function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var x = scope.estateImages.length
                var mustadded = { image: e.target.result, type: (scope.imageTypes)[0] };

                scope.estateImages.push(mustadded);
                scope.$apply();
            }

            elem.on('change', function () {
                reader.readAsDataURL(elem[0].files[0]);
            });
        }
    };
}])
.directive('myunitUpload', [function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var x = scope.unitImagetmp.length
                var mustadded = { image: e.target.result, type: (scope.imageTypes)[0], index:x };

                scope.unitImagetmp.push(mustadded);
                scope.$apply();
            }
            
            elem.on('change', function () {
                reader.readAsDataURL(elem[0].files[0]);
            });
        }
    };
}]);
