﻿var app = angular.module('customerApp', []);


var CustomersController = function ($scope, $http) {
    $scope.showModal = false;
    $scope.editedType = "";
    $scope.toggleModal = function (btnClicked) {
        $scope.editedType = btnClicked;
        $scope.showModal = !$scope.showModal;
    };

    $http.get("http://localhost:8080/REAgency/webapi/type/EstateType")
                   .success(function (response) { $scope.customers = response; });
    $scope.sortBy = 'type';
    $scope.reverse = false;
    //$scope.customers = getCustomers();
    $scope.doSort = function (propName) {
        $scope.sortBy = propName;
        $scope.reverse = !$scope.reverse
    };
    $scope.DeleteRow = function (index, typeId) {

        $scope.customers.splice(index, 1);
        var x = typeId;
        $http.get("http://localhost:8080/REAgency/webapi/type/delete/" + x)
                           .success(function (response) { });
    }
    $scope.EditRow = function (type) {
    
      
        type.type = $scope.editedType
        $http.post("http://localhost:8080/REAgency/webapi/type/update/type", type)
                           .success(function (response) { });
        //$scope.showModal = false;
    }



};

CustomersController.$inject = ['$scope', '$http'];
app.controller("CustomersController", CustomersController);

app.directive('modal', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
              '<div class="modal-content">' +
                '<div class="modal-header">' +
                  '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                  '<h4 class="modal-title">Type\'s Edit</h4>' +
                  '<input type="text" name="name" value="{{ editedType }}" ng-model="editedType"/>' +
                '</div>' +
                '<div class="modal-body" ng-transclude></div>' +
              '</div>' +
            '</div>' +
          '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
