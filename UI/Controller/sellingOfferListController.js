﻿//var selling = angular.module('sellingOfferList', ['kendo.directives', 'ui.bootstrap', 'ngAnimate','google-maps'])

selling.factory('MarkerCreatorService', function () {

    var markerId = 0;

    function create(latitude, longitude) {
        var marker = {
            options: {
                animation: 1,
                labelAnchor: "28 -5",
                labelClass: 'markerlabel'
            },
            latitude: latitude,
            longitude: longitude,
            id: ++markerId
        };
        return marker;
    }

    function invokeSuccessCallback(successCallback, marker) {
        if (typeof successCallback === 'function') {
            successCallback(marker);
        }
    }

    function createByCoords(latitude, longitude, successCallback) {
        var marker = create(latitude, longitude);
        invokeSuccessCallback(successCallback, marker);
    }

    function createByAddress(address, successCallback) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var firstAddress = results[0];
                var latitude = firstAddress.geometry.location.lat();
                var longitude = firstAddress.geometry.location.lng();
                var marker = create(latitude, longitude);
                invokeSuccessCallback(successCallback, marker);
            } else {
                alert("Unknown address: " + address);
            }
        });
    }

    function createByCurrentLocation(successCallback) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var marker = create(position.coords.latitude, position.coords.longitude);
                invokeSuccessCallback(successCallback, marker);
            });
        } else {
            alert('Unable to locate current position');
        }
    }

    return {
        createByCoords: createByCoords,
        createByAddress: createByAddress,
        createByCurrentLocation: createByCurrentLocation
    };

}).controller('ModalInstanceCtrlshowEstateImage', function ($scope, $modalInstance, items) {
    $scope.image = items.image;
}).controller('sellingOfferListController', function (MarkerCreatorService, $scope, $http, $modal, $log, $timeout) {

    $scope.selectCountry = $http.get("http://localhost:8080/REAgency/webapi/location/countries")
               .success(function (response) { $scope.countries = response; });
    $scope.seslectEstetType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateType")
              .success(function (response) { $scope.estateTypes = response; });
    $scope.selectPieceType = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
          .success(function (response) { $scope.pieceTypes = response; });
    $scope.selectsellingType = $http.get("http://localhost:8080/REAgency/webapi/type/SellingOfferType")
         .success(function (response) { $scope.sellingOfferTypes = response; });
    $scope.seslectFeatureType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateFeatureType")
              .success(function (response) { $scope.featureTypes = response; });
    $scope.seslectFacilityType = $http.get("http://localhost:8080/REAgency/webapi/type/FacilityType")
             .success(function (response) { $scope.facilityTypes = response; });

    $scope.countrySelected = function (id) {
        $scope.countyId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/state/" + id)
               .success(function (response) { $scope.states = response; });
    }
    $scope.stateSelected = function (id) {
        $scope.stateId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/city/" + id)
               .success(function (response) { $scope.cities = response; });
    }
    $scope.citySelected = function (id) {
        $scope.cityId = id;
        $http.get("http://localhost:8080/REAgency/webapi/location/district/" + id)
               .success(function (response) { $scope.districts = response; });
    }
    $scope.estateTypeSelected = function (estettype) {
        $scope.exampleEstateType = estettype;
    }; //
    //esellingOfferSelected(sellingOfferType)
    $scope.esellingOfferSelected = function (sellingType) {
        $scope.exampleSellingOfferType = sellingType;
    };
    $scope.conditionOptions = [{ 'option': ">= (Greater Than or Equal)" }, { 'option': "<= (Less Than or Equal)" }, { 'option': "= (Equal)" }];
    
    //regDateSelected(regDate)
    $scope.regDateSelected = function (date) {
        $scope.regDate = date;
    }
    //priceConditionSelected(priceCondition)
    $scope.priceConditionSelected = function (condition) {
        $scope.examplePriceCondition = condition;
    };
    //regDateConditionSelected(regdateOption)
    $scope.regDateConditionSelected = function (condition) {
        $scope.exampleRegDateCondition = condition;
    };
    //ageConditionSelected(ageCondition)
    $scope.ageConditionSelected = function (condition) {
        $scope.exampleAgeCondition = condition;
    };
    $scope.featuresExample = [];
    //featureSelect(feature)
    $scope.featureSelect = function (feature) {
        $scope.featuresExample.push(feature);
    };
    $scope.facilitiesExample = [];
    //facilitySelect(feature)
    $scope.facilitySelect = function (facility) {
        $scope.facilitiesExample.push(facility);
    };
    $scope.result = true;
    $scope.advanceSerch = false;
    $scope.details = false;
    $scope.sortBy = 'price';
    $scope.reverse = false;
    $scope.doSort = function (propName) {
        $scope.sortBy = propName;
        $scope.reverse = !$scope.reverse
    };
    $scope.QueryByExample = function () {
        //>= (Greater Than or Equal)
        //<= (Less Than or Equal)</o
        //= (Equal)</option>
        // $scope.exampleEstateType   $scope.exampleSellingOfferType     $scope.examplePriceCondition 
        // $scope.exampleAgeCondition  $scope.featuresExample  $scope.facilitiesExample
        var exampleObj = {};
        if ($scope.district != null) {
            exampleObj.districtId = $scope.district.id
        } else {
            exampleObj.districtId = 0;
        };
        if ($scope.exampleEstateType != null) {
            exampleObj.exampleEstateType = $scope.exampleEstateType
        } else {
            exampleObj.exampleEstateType = null;
        };
        if ($scope.exampleSellingOfferType) {
            exampleObj.exampleSellingOfferType = $scope.exampleSellingOfferType;
        } else {
            exampleObj.exampleSellingOfferType = null;
        };
        if ($scope.examplePriceCondition != null) {
            exampleObj.examplePriceCondition = $scope.examplePriceCondition.option;
        } else {
            exampleObj.examplePriceCondition != "";
        };
        if ($scope.exampleAgeCondition != null) {
            exampleObj.exampleAgeCondition = $scope.exampleAgeCondition.option;
        } else {
            exampleObj.exampleAgeCondition = "";
        };
        if ($scope.featuresExample != null) {
            exampleObj.featuresExample = $scope.featuresExample;
        } else {
            exampleObj.featuresExample = null;
        };
        if ($scope.facilitiesExample != null) {
            exampleObj.facilitiesExample = $scope.facilitiesExample;
        } else {
            exampleObj.facilitiesExample = null;
        };
        if ($scope.pieceNum != null) {
            exampleObj.pieceNum = $scope.pieceNum;
        } else {
            exampleObj.pieceNum = 0;
        };
        if ($scope.age != null) {
            exampleObj.age = $scope.age;
        } else {
            exampleObj.age = 0;
        };
        if ($scope.exampleRegDateCondition != null) {
            exampleObj.exampleRegDateCondition = $scope.exampleRegDateCondition.option;
        } else {
            exampleObj.exampleRegDateCondition = "";
        };
        if ($scope.regDate != null) {
            exampleObj.regDate = $scope.regDate;
        } else {
            exampleObj.regDate = null;
        };
        if ($scope.basePrice != null) {
            exampleObj.basePrice = $scope.basePrice;
        } else {
            exampleObj.basePrice = 0;
        };
        $http.post("http://localhost:8080/REAgency/webapi/sellingOffer/example", exampleObj,false)
                       .success(function (response) {
                           $scope.sellingOfferResult = response;

                           $scope.sellingOffers = $scope.sellingOfferResult;
                           $scope.details = false;
                           $scope.advanceSerch = false;
                           $scope.result = true;
                       });
       

        $scope.district = null;
        $scope.exampleEstateType = null;
        $scope.exampleSellingOfferType = null;
        $scope.examplePriceCondition =  null;
        $scope.exampleAgeCondition =  null; 
        $scope.featuresExample = null;

        $scope.facilitiesExample = [];
        $scope.featuresExample = [];
        $scope.pieceNum = 0;
        $scope.age = 0,
        $scope.exampleRegDateCondition =  null;
        $scope.regDate = null; 
        $scope.basePrice = 0;

      

    };
    $scope.refreshTest = function () {
        $http.get("http://localhost:8080/REAgency/webapi/sellingOffer/all")
             .success(function (response) {
                 $scope.sellingOffersResultALL = response;
                 $scope.sellingOffers = $scope.sellingOffersResultALL;
                 $scope.advanceSerch = false;
                 $scope.result = true;
             });
       

    };
    $scope.showAdvanceSearch = function () {
        $scope.selectCountry = $http.get("http://localhost:8080/REAgency/webapi/location/countries")
              .success(function (response) { $scope.countries = response; });
        $scope.seslectEstetType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateType")
                  .success(function (response) { $scope.estateTypes = response; });
        $scope.selectPieceType = $http.get("http://localhost:8080/REAgency/webapi/type/PieceType")
              .success(function (response) { $scope.pieceTypes = response; });
        $scope.selectsellingType = $http.get("http://localhost:8080/REAgency/webapi/type/SellingOfferType")
             .success(function (response) { $scope.sellingOfferTypes = response; });
        $scope.seslectFeatureType = $http.get("http://localhost:8080/REAgency/webapi/type/EstateFeatureType")
                  .success(function (response) { $scope.featureTypes = response; });
        $scope.seslectFacilityType = $http.get("http://localhost:8080/REAgency/webapi/type/FacilityType")
                 .success(function (response) { $scope.facilityTypes = response; });
       
        $scope.advanceSerch = true;
        $scope.result = false;
    };
   
    $scope.ShowSellingOfferDetail = function (sellingOfferId) {

        $scope.details = true;
        $http.get(" http://localhost:8080/REAgency/webapi/sellingOffer/details/" + sellingOfferId)
              .success(function(response) {
                $scope.sellingOfferDetails = response;
                window.scrollTo(0, document.body.scrollHeight);
            });


        ///get country by id
        //$http.get(" http://localhost:8080/REAgency/webapi/sellingOffer/details/" + sellingOfferId)
        //      .success(function (response) { $scope.sellingOfferDetails = response; });

        ///get state by id
        //$http.get(" http://localhost:8080/REAgency/webapi/sellingOffer/details/" + sellingOfferId)
        //      .success(function (response) { $scope.sellingOfferDetails = response; });
        ///get city by id
        //$http.get(" http://localhost:8080/REAgency/webapi/sellingOffer/details/" + sellingOfferId)
        //      .success(function (response) { $scope.sellingOfferDetails = response; });
        ///get district by id
        //$http.get(" http://localhost:8080/REAgency/webapi/sellingOffer/details/" + sellingOfferId)
        //      .success(function (response) { $scope.sellingOfferDetails = response; });



    };

    $scope.showUnit = function (index) {

        $scope.unit = $scope.sellingOfferDetails.units[index];

        var modalInstance = $modal.open({
            templateUrl: 'myModalAddUnitContent.html',
            controller: 'ModalInstanceCtrlnewUnitShow',
            size: 'lg',
            resolve: {
                items: function () {
                    return { 'unit': $scope.unit};
                }
            }
        });

        modalInstance.result.then(function () {
           
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    

    $scope.AddBuyingOffer = function (offerId) {

        $scope.details = false;
        $scope.selectedOfferId = offerId;

        var modalInstance = $modal.open({
            templateUrl: 'myModalAddOffer.html',
            controller: 'ModalInstanceCtrlnewOffer',
            size: 'sm',
            resolve: {
                items: function () {
                    return { 'offerId': $scope.selectedOfferId };
                }
            }
        });

        modalInstance.result.then(function () {
           
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
        //// show popup
    };
    /////////////////////////////////map part
    MarkerCreatorService.createByCoords(45.497183, -73.578995, function (marker) {
        marker.options.labelContent = 'Montreal';
        $scope.autentiaMarker = marker;
    });

    $scope.address = '';

    $scope.map = {
        center: {
            latitude: $scope.autentiaMarker.latitude,
            longitude: $scope.autentiaMarker.longitude
        },
        zoom: 12,
        markers: [],
        control: {},
        options: {
            scrollwheel: false
        }
    };

    $scope.map.markers = [$scope.autentiaMarker];



    $scope.addAddress = function () {
        var address = $scope.address;
        if (address !== '') {
            MarkerCreatorService.createByAddress(address, function (marker) {
                $scope.map.markers = [marker];
                refresh(marker);
            });
        }
    };

    function refresh(marker) {
        $scope.map.control.refresh({
            latitude: marker.latitude,
            longitude: marker.longitude
        });
    }




})
.controller('ModalInstanceCtrlnewUnitShow', function ($scope, $http, $modalInstance, items) {


    $scope.unit = items.unit;
    if ($scope.unit.pieces != null) {
        $scope.piecetmp = $scope.unit.pieces;
    }
    if ($scope.unit.appliances != null) {
        $scope.appliancetmp = $scope.unit.appliances;
    }
    $scope.sliderunitImageIndex = 0;
    $scope.sliderImage;
    $scope.unitImagetmp = $scope.unit.unitImages;

    $scope.slidervisible = false;
    $scope.showUnitImage = function (um) {
        $scope.sliderunitImageIndex = um.index;
        $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        $scope.slidervisible = true;
    };//
    // show prev image
    $scope.showPrev = function () {
        if ($scope.sliderunitImageIndex > 0) {
            $scope.sliderunitImageIndex--;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        } else {
            $scope.sliderunitImageIndex = $scope.unitImagetmp.length - 1;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        }
    };
    //showUnitImage(um)
    // show next image
    $scope.showNext = function () {
        if ($scope.sliderunitImageIndex < $scope.unitImagetmp.length - 1) {
            $scope.sliderunitImageIndex++;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        } else {
            $scope.sliderunitImageIndex = 0;
            $scope.sliderImage = $scope.unitImagetmp[$scope.sliderunitImageIndex].image;
        }
    };



})
.controller('ModalInstanceCtrlnewOffer', function ($scope, $http, $modalInstance, items) {

    $scope.selectedOfferId = items.offerId;
    
    $scope.saveOffer = function (offerId) {
        var offerId = $scope.selectedOfferId
        var name = $scope.name;
        var family = $scope.family;
        var email = $scope.email;
        var phone = $scope.phone;
        var offerPrice = $scope.offeredPrice;
        var offerExtraInfo = $scope.offerExtraInfo;
        var offerObj = {
            'offerId': offerId,
            'name': name,
            'family': family,
            'email': email,
            'phone': phone,
            'offerPrice': offerPrice,
            'offerExtraInfo': offerExtraInfo
        };
        $http.post("http://localhost:8080/REAgency/webapi/offer/newOffer", offerObj)
                       .success(function (response) { $scope.buyingOfferResult = response });

        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
   
})