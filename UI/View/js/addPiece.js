﻿function makeElements(){
    var iCnt = 1;

// CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
var container = $(document.createElement('div')).css({
    padding: '5px', margin: '20px', width: '255px', border: '1px dashed',
    borderTopColor: '#999', borderBottomColor: '#999',
    borderLeftColor: '#999', borderRightColor: '#999'
});

$('#btAddPiece').click(function() {
   
    if(iCnt == 0){
        iCnt = 1;
    }
       

        // ADD TEXTBOX.
    $(container).append('<div id=tb' + iCnt + '><label  title="Piece Type" id=tb' + iCnt + '>Piece Type:</label><br />' +
        '<select ng-model="featureType" id=tb' + iCnt + ' ></select><br/>'+
        '<div id=tb' + iCnt + '><label  title="Quantity" id=tb' + iCnt + '>Quantity:</label><br />' +
         '<input type="text" name="name" value=" " id=tb' + iCnt + '/><br /></div>'+
         '<label title="Extra Information" class="title" id=tb' + iCnt + '>Description:</label><br />' +
         '<textarea id="container" ng-model="piecedescription" id=tb' + iCnt + '></textarea>');

    

    
       
       // GetTextValue();
        //if (iCnt == 1) {        // SHOW SUBMIT BUTTON IF ATLEAST "1" ELEMENT HAS BEEN CREATED.

        //    var divSubmit = $(document.createElement('div'));
        //    $(divSubmit).append('<input type=button class="bt" onclick="GetTextValue()"' + 
        //            'id=btSubmit value=Submit />');

        //}

        $('#pieces').after(container);   // ADD BOTH THE DIV ELEMENTS TO THE "features" CONTAINER.
        iCnt++;
});

$('#btRemove').click(function() {   // REMOVE ELEMENTS ONE PER CLICK.
    if (iCnt != 0) { $('#tb' + iCnt).remove(); iCnt = iCnt - 1; }
        
    if (iCnt == 0) { $(container).empty(); 
        
        $(container).remove(); 
        
        $('#btAddPiece').removeAttr('disabled'); 
        $('#btAddPiece').attr('class', 'bt') 

    }
});

$('#btRemoveAll').click(function() {    // REMOVE ALL THE ELEMENTS IN THE CONTAINER.
        
    $(container).empty(); 
    $(container).remove(); 
    $('#btSubmit').remove(); iCnt = 0; 
    $('#btAddPiece').removeAttr('disabled'); 
    $('#btAddPiece').attr('class', 'bt');

});
}

// PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
var divValue, values = '';

function GetTextValue() {

    $(divValue).empty(); 
    $(divValue).remove(); values = '';

    $('.input').each(function() {
        divValue = $(document.createElement('div')).css({
            padding:'5px', width:'200px'
        });
        values += this.value + '<br />'
    });

    $(divValue).append('<p><b>Your selected values</b></p>' + values);
    $('body').append(divValue);
}