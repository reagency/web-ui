﻿function makeNearby() {
    var iCntN= 1;

    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
    var containerN = $(document.createElement('div')).css({
        padding: '5px', margin: '20px', width: '170px', border: '1px dashed',
        borderTopColor: '#999', borderBottomColor: '#999',
        borderLeftColor: '#999', borderRightColor: '#999'
    });

    $('#btAddNearby').click(function () {

        if (iCntN == 0) {
            iCntN = 1;
        }


        // ADD TEXTBOX.
        $(containerN).append('<div id=tbN' + iCntN + '><label  title="Nearby Facility Type" id=tbN' + iCntN + '>Nearby Facility Type:</label><br />' +
            '<select ng-model="nearbyFacilityType" id=tbN' + iCntN + ' ></select><br/>' +
            '<div id=tbN' + iCntN + '><label  title="Nearby Facility value" id=tbN' + iCntN + '>value:</label><br />' +
             '<input type="text" name="name" value=" " id=tbN' + iCntN + '/><br /></div>' +
             '<label  title="Nearby Facility unit" id=tbN' + iCntN + '>unit:</label><br />' +
             '<input type="text" name="name" value=" " id=tbN' + iCntN + '/><br /></div>'
             );




        // GetTextValue();
        //if (iCntN == 1) {        // SHOW SUBMIT BUTTON IF ATLEAST "1" ELEMENT HAS BEEN CREATED.

        //    var divSubmit = $(document.createElement('div'));
        //    $(divSubmit).append('<input type=button class="bt" onclick="GetTextValue()"' + 
        //            'id=btSubmit value=Submit />');

        //}

        $('#nearby').after(containerN);   // ADD BOTH THE DIV ELEMENTS TO THE "features" containerN.
        iCntN++;
    });

    $('#btRemoveN').click(function () {   // REMOVE ELEMENTS ONE PER CLICK.
        if (iCntN != 0) { $('#tbN' + iCntN).remove(); iCntN = iCntN - 1; }

        if (iCntN == 0) {
            $(containerN).empty();

            $(containerN).remove();

            $('#btAddNearby').removeAttr('disabled');
            $('#btAddNearby').attr('class', 'k-primary N')

        }
    });

    $('#btRemoveAllN').click(function () {    // REMOVE ALL THE ELEMENTS IN THE containerN.

        $(containerN).empty();
        $(containerN).remove();
        iCntN = 0;
        $('#btAddNearby').removeAttr('disabled');
        $('#btAddNearby').attr('class', 'k-primary N');

    });
}

// PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
var divValue, values = '';

function GetTextValue() {

    $(divValue).empty();
    $(divValue).remove(); values = '';

    $('.input').each(function () {
        divValue = $(document.createElement('div')).css({
            padding: '5px', width: '200px'
        });
        values += this.value + '<br />'
    });

    $(divValue).append('<p><b>Your selected values</b></p>' + values);
    $('body').append(divValue);
}