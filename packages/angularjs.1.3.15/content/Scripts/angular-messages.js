/**
 * @license AngularJS v1.3.15
 * (c) 2010-2014 Google, Inc. http://angularjs.org
 * License: MIT
 */
(function(window, angular, undefined) {'use strict';

/**
 * @ngdoc module
 * @name ngMessages
 * @description
 *
 * The `ngMessages` module provides enhanced support for displaying messages within templates
 * (typically within forms or when rendering message objects that return key/value data).
 * Instead of relying on JavaScript code and/or complex ng-if statements within your form template to
 * show and hide error messages specific to the state of an input field, the `ngMessages` and
 * `ngMessage` directives are designed to handle the complexity, inheritance and priority
 * sequencing based on the order of how the messages are defined in the template.
 *
 * Currently, the ngMessages module only contains the code for the `ngMessages`
 * and `ngMessage` directives.
 *
 * # Usage
 * The `ngMessages` directive listens on a key/value collection which is set on the ngMessages attribute.
 * Since the {@link ngModel ngModel} directive exposes an `$error` object, this error object can be
 * used with `ngMessages` to display control error messages in an easier way than with just regular angular
 * template directives.
 *
 * ```html
 * <form name="myForm">
 *   <input type="text" ng-model="field" name="myField" required minlength="5" />
 *   <div ng-messages="myForm.myField.$error">
 *     <div ng-message="required">You did not enter a field</div>
 *     <div ng-message="minlength">The value entered is too short</div>
 *   </div>
 * </form>
 * ```
 *
 * Now whatever key/value entries are present within the provided object (in this case `$error`) then
 * the ngMessages directive will render the inner first ngMessage directive (depending if the key values
 * match the attribute value present on each ngMessage directive). In other words, if your errors
 * object contains the following data:
 *
 * ```javascript
 * <!-- keep in mind that ngModel automatically sets these error flags -->
 * myField.$error = { minlength : true, required : false };
 * ```
 *
 * Then the `required` message will be displayed first. When required is false then the `minlength` message
 * will be displayed right after (since these messages are ordered this way in the template HTML code).
 * The prioritization of each message is determined by what order they're present in the DOM.
 * Therefore, instead of having custom JavaScript code determine the priority of what errors are
 * present before others, the presentation of the errors are handled within the template.
 *
 * By default, ngMessages will only display one error at a time. However, if you wish to display all
 * messages then the `ng-messages-multiple` attribute flag can be used on the element containing the
 * ngMessages directive to make this happen.
 *
 * ```html
 * <div ng-messages="myForm.myField.$error" ng-messages-multiple>...</div>
 * ```
 *
 * ## Reusing and Overriding Messages
 * In addition to prioritization, ngMessages also allows for including messages from a remote or an inline
 * template. This allows for generic collection of messages to be reused across multiple parts of an
 * application.
 *
 * ```html
 * <script type="text/ng-template" id="error-messages">
 *   <div ng-message="required">This field is required</div>
 *   <div ng-message="minlength">This field is too short</div>
 * </script>
 * <div ng-messages="myForm.myField.$error" ng-messages-include="error-messages"></div>
 * ```
 *
 * However, including generic messages may not be useful enough to match all input fields, therefore,
 * `ngMessages` provides the ability to override messages defined in the remote template by redefining
 * then within the directive container.
 *
 * ```html
 * <!-- a generic template of error messages known as "my-custom-messages" -->
 * <script type="text/ng-template" id="my-custom-messages">
 *   <div ng-message="required">This field is required</div>
 *   <div ng-message="minlength">This field is too short</div>
 * </script>
 *
 * <form name="myForm">
 *   <input type="email"
 *          id="INDX( 	 �$           (   @  �       �    S ~ 6         ��    h X     ��    ��{�v��s���k��u�A�����{�v��       �	              A N 5 B 1 7 ~ 1 . J S ��    h X     ��    �H�v�� ��k����A����H�v��                      A N 5 B 7 E ~ 1 . J S .�'    h X     ��    ��v��yr�k����B�����v��       W	              A N 5 C 9 3 ~ 1 . J S j�'    h X     ��    Li}�v��>���k����B���Li}�v��       �	              A N 5 C A B ~ 1 . J S ��'    h X     ��    ��+�v� :���k�T�$C�����+�v��       �	              A N 5 C E 3 ~ 1 . J S ��'    h X     ��    I��v���d��k��OnB���I��v��       `
              A N 5 D 6 9 ~ 1 . J S 9�    h X     ��    T��v�����k�?��A���T��v��       Y	              A N 5 D 8 7 ~ 1 . J S ��    h X     ��    ��H�v��ǻ��k��k�A�����H�v��       �              A N 5 D 8 C ~ 1 . J S ��'    h X     ��    ���v���6�k�
6�B������v��       �	              A N 5 D E 4 ~ 1 . J S ��'    h X     ��    �v��v�����k�~C����v��v��       �	              A N 5 D E E ~ 1 . J S g�'    h X     ��    &>x�v��	���k���B���&>x�v��       m	              A N 5 E 2 9 ~ 1 . J S ��    h X     ��    dD�v��A���k���OB���dD�v��       w	              A N 5 E B 5 ~ 1 . J S 	�    h X     ��    �4��v������k����A����4��v��       
              A N 5 F 0 2 ~ 1 . J S ��    h X     ��    n8�v�����k�_vzA���n8�v��       &
              A N 6 0 E D ~ 1 . J S ��    h X    ��    ��-�v��x��k����A�����-�v��       �              A N 6 1 3 E ~ 1 . J S ��    h X     ��    q�W�v��<_��k��VB���q�W�v��       �
              A N 6 1 D 4 ~ 1 . J S �    h X     ��    n���v��!��k�ש�A���n���v��       �
              A N 6 2 2 1 ~ 1 . J S ��    h X     ��    0��v��\�n�k�3bA���0��v��       �              A N 6 2 3 2 ~ 1 . J S ��    h X     ��    ��v��.���k��A�����v��       �              A N 6 2 A 6 ~ 1 . J S ��    h X     ��    ����v��mox�k��iA�������v��       l              A N 6 2 A E ~ 1 . J S ��    h X     ��    ����v���sy�k�<�iA�������v��       �              A N 6 3 1 6 ~ 1 . J S ӟ'    h X     ��    ����v��+[�k�K�_C�������v��                     A N 6 3 5 2 ~ 1 . J S ��'    h X     ��    C}�v���a(�k�0�LC���C}�v��       &              A N 6 3 5 6 ~ 1 . J S !�    h X     ��    ����v�����k���A�������v��       Y	              A N 6 3 8 0 ~ 1 . J  ɟ'    h X     ��    �ז�v���K�k�aFTC����ז�v��                     A N 6 3 8 6 ~ 1 . J S v�'    h X     ��    ���v��@@��k�+��B������v��       (
              A N 6 3 F B ~ 1 . J S R�    h X     ��    �($�v���1�k�}f�A����($�v��       Z	              A N 6 4 1 6 ~ 1 . J S ��'    h X     ��    43��v���33�k�F��B���43��v��       h	              A N 6 4 6 9 ~ 1 . J S {�    h X     ��    �Mo�v��pB�k��AB����Mo�v��       '              A N 6 4 8 F  1 . J S ��    h X     ��    ���v��x���k�MT�A������v��                      A N 6 4 A B ~ 1 . J S ��    h X     ��    k���v��t^d�k��QA���k���v��       g              A N 6 5 C F ~ 1 . J S t�'    h X     ��    ct��v���o��k����B���ct��v��       }	              A N 6 6 7 A ~ 1 . J S ��    h X     ��    �Uf�v���H��k�h��A����Uf�v��       $
              A N 6 6 9 1 ~ 1 . J S ��    h X     ��    ����v���|�k�3�kA�������v��       �              A N  6 A 3 ~ 1 . J S ��    h X     ��    m�'�v��$���k�lCEB���m�'�v��       !
              A N 6 6 C E ~ 1 . J S  �'    h X     ��    �I��v��T:d�k�б�B����I��v��       v	              A N 6 6 F D ~ 1 . J S �    h X     ��    .d��v��},��k�	��A���.d��v��       
              A N 6 7 5 E ~ 1 . J S               ��    .d��v��},��k�	��A���.d��v��       
              A N 6 7 5 E ~ 1 . J S                                                                              lue2">...</ANY>
    *   <ANY ng-message="keyValue3">...</ANY>
    * </ANY>
    *
    * <!-- or by using element directives -->
    * <ng-messages for="expression">
    *   <ng-message when="keyValue1">...</ng-message>
    *   <ng-message when="keyValue2">...</ng-message>
    *   <ng-message when="keyValue3">...</ng-message>
    * </ng-messages>
    * ```
    *
    * @param {string} ngMessages an angular expression evaluating to a key/value object
    *                 (this is typically the $error object on an ngModel instance).
    * @param {string=} ngMessagesMultiple|multiple when set, all messages will be displayed with true
    * @param {string=} ngMessagesInclude|include when set, the specified template will be included into the ng-messages container
    *
    * @example
    * <example name="ngMessages-directive" module="ngMessagesExample"
    *          deps="angular-messages.js"
    *          animations="true" fixBase="true">
    *   <file name="index.html">
    *     <form name="myForm">
    *       <label>Enter your name:</label>
    *       <input type="text"
    *              name="myName"
    *              ng-model="name"
    *              ng-minlength="5"
    *              ng-maxlength="20"
    *              required />
    *
    *       <pre>myForm.myName.$error = {{ myForm.myName.$error | json }}</pre>
    *
    *       <div ng-messages="myForm.myName.$error" style="color:maroon">
    *         <div ng-message="required">You did not enter a field</div>
    *         <div ng-message="minlength">Your field is too short</div>
    *         <div ng-message="maxlength">Your field is too long</div>
    *       </div>
    *     </form>
    *   </file>
    *   <file name="script.js">
    *     angular.module('ngMessagesExample', ['ngMessages']);
    *   </file>
    * </example>
    */
  .directive('ngMessages', ['$compile', '$animate', '$templateRequest',
                   function($compile,    $animate,   $templateRequest) {
    var ACTIVE_CLASS = 'ng-active';
    var INACTIVE_CLASS = 'ng-inactive';

    return {
      restrict: 'AE',
      controller: function() {
        this.$renderNgMessageClasses = angular.noop;

        var messages = [];
        this.registerMessage = function(index, message) {
          for (var i = 0; i < messages.length; i++) {
            if (messages[i].type == message.type) {
              if (index != i) {
                var temp = messages[index];
                messages[index] = messages[i];
                if (index < messages.length) {
                  messages[i] = temp;
                } else {
                  messages.splice(0, i); //remove the old one (and shift left)
                }
              }
              return;
            }
          }
          messages.splice(index, 0, message); //add the new one (and shift right)
        };

        this.renderMessages = function(values, multiple) {
          values = values || {};

          var found;
          angular.forEach(messages, function(message) {
            if ((!found || multiple) && truthyVal(values[message.type])) {
              message.attach();
              found = true;
            } else {
              message.detach();
            }
          });

          this.renderElementClasses(found);

          function truthyVal(value) {
            return value !== null && value !== false && value;
          }
        };
      },
      require: 'ngMessages',
      link: function($scope, element, $attrs, ctrl) {
        ctrl.renderElementClasses = function(bool) {
          bool ? $animate.setClass(element, ACTIVE_CLASS, INACTIVE_CLASS)
               : $animate.setClass(element, INACTIVE_CLASS, ACTIVE_CLASS);
        };

        //JavaScript treats empty strings as false, but ng-message-multiple by itself is an empty string
        var multiple = angular.isString($attrs.ngMessagesMultiple) ||
                       angular.isString($attrs.multiple);

        var cachedValues, watchAttr = $attrs.ngMessages || $attrs['for']; //for is a reserved keyword
        $scope.$watchCollection(watchAttr, function(values) {
          cachedValues = values;
          ctrl.renderMessages(values, multiple);
        });

        var tpl = $attrs.ngMessagesInclude || $attrs.include;
        if (tpl) {
          $templateRequest(tpl)
            .then(function processTemplate(html) {
              var after, container = angular.element('<div/>').html(html);
              angular.forEach(container.children(), function(elm) {
               elm = angular.element(elm);
               after ? after.after(elm)
                     : element.prepend(elm); //start of the container
               after = elm;
               $compile(elm)($scope);
              });
              ctrl.renderMessages(cachedValues, multiple);
            });
        }
      }
    };
  }])


   /**
    * @ngdoc directive
    * @name ngMessage
    * @restrict AE
    * @scope
    *
    * @description
    * `ngMessage` is a directive with the purpose to show and hide a particular message.
    * For `ngMessage` to operate, a parent `ngMessages` directive on a parent DOM element
    * must be situated since it determines which messages are visible based on the state
    * of the provided key/value map that `ngMessages` listens on.
    *
    * More information about using `ngMessage` can be found in the
    * {@link module:ngMessages `ngMessages` module documentation}.
    *
    * @usage
    * ```html
    * <!-- using attribute directives -->
    * <ANY ng-messages="expression">
    *   <ANY ng-message="keyValue1">...</ANY>
    *   <ANY ng-message="keyValue2">...</ANY>
    *   <ANY ng-message="keyValue3">...</ANY>
    * </ANY>
    *
    * <!-- or by using element directives -->
    * <ng-messages for="expression">
    *   <ng-message when="keyValue1">...</ng-message>
    *   <ng-message when="keyValue2">...</ng-message>
    *   <ng-message when="keyValue3">...</ng-message>
    * </ng-messages>
    * ```
    *
    * @param {string} ngMessage a string value corresponding to the message key.
    */
  .directive('ngMessage', ['$animate', function($animate) {
    var COMMENT_NODE = 8;
    return {
      require: '^ngMessages',
      transclude: 'element',
      terminal: true,
      restrict: 'AE',
      link: function($scope, $element, $attrs, ngMessages, $transclude) {
        var index, element;

        var commentNode = $element[0];
        var parentNode = commentNode.parentNode;
        for (var i = 0, j = 0; i < parentNode.childNodes.length; i++) {
          var node = parentNode.childNodes[i];
          if (node.nodeType == COMMENT_NODE && node.nodeValue.indexOf('ngMessage') >= 0) {
            if (node === commentNode) {
              index = j;
              break;
            }
            j++;
          }
        }

        ngMessages.registerMessage(index, {
          type: $attrs.ngMessage || $attrs.when,
          attach: function() {
            if (!element) {
              $transclude($scope, function(clone) {
                $animate.enter(clone, null, $element);
                element = clone;
              });
            }
          },
          detach: function(now) {
            if (element) {
              $animate.leave(element);
              element = null;
            }
          }
        });
      }
    };
  }]);


})(window, window.angular);
